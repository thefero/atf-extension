window.$ = window.jQuery = require('jquery');
import 'jquery-ui-dist/jquery-ui.js';

chrome.runtime.sendMessage({
    from: 'popup'
    , type: 'getCommands'
}, function(commands) {
    let commandsList = $('#container > .content > .atf-rec-commands-container > .atf-rec-commands');
    commandsList.empty();

    commands.forEach(function(command) {
        let value = command.value ? `, "${command.value}"` : ``;

        commandsList.append(
            $('<li>').addClass('atf-rec-command').html(`
                <span class="sort-handle"><i class="fa fa-bars" aria-hidden="true"></i></span>$I->${command.action}("<span class="atf-rec-selector">${command.selector}</span>"<span class="atf-rec-value">${value}</span>);
                <div class="atf-rec-command-actions"><button class="atf-rec-floating-button atf-rec-del"><i class="fa fa-times" aria-hidden="true"></i></button></div>
            `)
        );

        $('.atf-rec-commands').find('.atf-rec-command:last').data('original', command);

        $('.atf-rec-commands').find('.atf-rec-command:last .atf-rec-del')
            .off('click')
            .on('click', function(ev) {
                let _this = $(this);

                chrome.runtime.sendMessage({
                    from: 'popup'
                    , type: 'removeCommand'
                    , command: _this.closest('.atf-rec-command').data('original')
                });

                _this.closest("li").remove();
            });

        $('.atf-rec-commands').find('.atf-rec-command:last .atf-rec-selector')
            .off('dblclick')
            .on('dblclick', function(ev) {
                let _this = $(this);

                if (!_this.find('input').length) {
                    _this.html(
                        $("<input>").val(_this.text())
                            .off('blur')
                            .on('blur', function() {
                                let thisVal = $(this).val();
                                $(this).replaceWith($(this).val());

                                let newCommand = {
                                    action: _this.closest('.atf-rec-command').data('original').action
                                    , selector: thisVal
                                    , value: _this.closest('.atf-rec-command').data('original').value
                                };

                                chrome.runtime.sendMessage({
                                    from: 'popup'
                                    , type: 'changeCommand'
                                    , originalCommand: _this.closest('.atf-rec-command').data('original')
                                    , newCommand: newCommand
                                });

                                _this.closest('.atf-rec-command').data('original', newCommand);
                            })
                    );
                }

            });
    })
});

chrome.runtime.sendMessage({
    type: 'getStatus'
}, function (status) {
    toggleActivateButton(status);
});

let toggleActivateButton = (status) => {
    if (status) {
        $('#deactivate-extension').show();
        $('#activate-extension').hide();
    } else {
        $('#deactivate-extension').hide();
        $('#activate-extension').show();
    }
}

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (typeof msg.type !== 'undefined' && msg.type === 'status') {
        toggleActivateButton(msg.value);
    }
});

$('#activate-extension')
    .off('click')
    .on('click', function(ev) {
        chrome.runtime.sendMessage({
            type: 'setStatus'
            , value: true
        }, (response) => {
            toggleActivateButton(response);
        });
    });

$('#deactivate-extension')
    .off('click')
    .on('click', function(ev) {
        chrome.runtime.sendMessage({
            type: 'setStatus'
            , value: false
        }, (response) => {
            toggleActivateButton(response);
        });
    });

$('#container > .content > .atf-rec-commands-container > .atf-rec-commands').sortable({
    handle: '.sort-handle'
    , stop: function(ev, ui) {
        let newCommands = [];
        $('.atf-rec-commands').find('.atf-rec-command').each(function(index, el) {
            newCommands.push($(el).data('original'));
        })

        chrome.runtime.sendMessage({
            from: 'popup'
            , commands: newCommands
        }, (response) => {
        });
    }
});