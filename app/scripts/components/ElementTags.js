const $ = jQuery = require('jquery');
import Tooltip from 'tooltip.js';
window.Tooltip = Tooltip;

let template = $(`
    <div class="atf-rec-element-tags">
        <div class="atf-rec-box-title">Element attributes</div>
        <div class="atf-rec-element-tags-list-container">
            <div class="atf-rec-list-h-scroll-button-container">
                <button role="button" class="atf-rec-scroll-button atf-rec-go-left atf-rec-disabled"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            </div>
            <ul class="atf-rec-element-tags-list"></ul>
            <div class="atf-rec-list-h-scroll-button-container">
                <button role="button" class="atf-rec-scroll-button atf-rec-go-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
`);

export default class ElementTag {
    constructor(properties = {}) {
        this.template = template.clone();
        this.tagsListTemplate = this.template.find('.atf-rec-element-tags-list')
        this.currnetListPosition = 0;

        this.attributes = [];

        this.properties = Object.assign({
            container: $('body')
        }, properties);

        this.attributes = this.__getElementsTags(this.properties.element);

        this.__displayTags();

        let boxTitle = `<${this.properties.element.tagName.toLowerCase()}`;
        this.attributes.forEach(attr => {
            boxTitle += ` ${attr.node}="${attr.value}"`;
        });
        boxTitle += ` />`;

        this.properties.container.empty().html(this.template);
        this.template.find('.atf-rec-box-title')
            .text(boxTitle);

        this.scrollWidth = 200;
        if (this.tagsListTemplate[0].scrollWidth == Math.floor(this.tagsListTemplate.width())) {
            this.scrollWidth = this.tagsListTemplate.width();
        }

        this.tick = Math.floor(this.tagsListTemplate[0].scrollWidth / this.scrollWidth);
        this.tickCounter = 0;

        this.__bindListeners();
        return this;
    }

    __displayTags() {
        this.attributes.forEach(attribute => {
            attribute.html = $(`
                <li class="atf-rec-nav-tab">
                    <a class="atf-rec-nav-tabs-action" href="#">
                        <span class="atf-rec-element-tag">
                            ${attribute.node}
                        </span>
                        <span class="atf-rec-element-value ${!attribute.value ? 'atf-rec-no-text': ''}">${attribute.value || 'No value'}</span>
                    </a>
                </li>
            `);

            this.tagsListTemplate.append(attribute.html);

            new Tooltip(
                attribute.html.find('.atf-rec-nav-tabs-action')
                , {
                    placement: 'top'
                    , html: true
                    , title: `<div class="atf-rec-attribute-code-tip">${attribute.node}</div><div>${attribute.value || 'No value'}</div>`
                }
            );
        });
    }

    clicked() {
        alert('xxx');
        return false;
    }

    __getElementsTags(element) {
        let nodes = []
            , values = []
            , elementsAttributes = [];

        for (let i = 0, attrs = element.attributes, n = attrs.length; i < n; i++) {
            elementsAttributes.push({
                node: attrs[i].nodeName
                , value: attrs[i].nodeValue
            });
        }

        return elementsAttributes;
    }

    __bindListeners() {
        this.attributes.forEach(attribute => {
            attribute.html.find('.atf-rec-nav-tabs-action')
                .off('click.atf-rec')
                .on('click.atf-rec', function() {
                    return false;
                })
        });

        this.template.find('.atf-rec-scroll-button.atf-rec-go-right')
            .off('click.atf-rec')
            .on('click.atf-rec', function(ev) {
                if (this.tickCounter >= this.tick) {
                    return false;
                }

                this.template.find('.atf-rec-go-left').removeClass('atf-rec-disabled');

                this.tickCounter++;
                this.currnetListPosition -= this.scrollWidth;

                this.tagsListTemplate.css({
                    marginLeft: this.currnetListPosition
                });

                if (this.tickCounter + 1 >= this.tick) {
                    this.template.find('.atf-rec-go-right').addClass('atf-rec-disabled');
                }

                return false;
            }.bind(this));

        this.template.find('.atf-rec-scroll-button.atf-rec-go-left')
            .off('click.atf-rec')
            .on('click.atf-rec', function(ev) {
                if (this.tickCounter <= 0) {
                    return false;
                }
                
                this.template.find('.atf-rec-go-right').removeClass('atf-rec-disabled');

                this.tickCounter--;
                this.currnetListPosition += this.scrollWidth;
                
                this.tagsListTemplate.css({
                    marginLeft: this.currnetListPosition
                });

                if (this.tickCounter - 1 <= 0) {
                    this.template.find('.atf-rec-go-left').addClass('atf-rec-disabled');
                }

                return false;
            }.bind(this));
    }
}