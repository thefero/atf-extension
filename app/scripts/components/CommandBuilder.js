const $ = jQuery = require('jquery');
import 'jquery-ui-dist/jquery-ui.js';
import EventBus from '../components/EventBus';
import vars from '../utils/variables';
import See from '../components/Commands/See';
import PressKey from '../components/Commands/PressKey';
import Click from '../components/Commands/Click';
import SeeInField from '../components/Commands/SeeInField';
import FillField from '../components/Commands/FillField';
import SelectOption from '../components/Commands/SelectOption';
import SeeCheckBoxIsChecked from '../components/Commands/SeeCheckBoxIsChecked';
import CheckOption from '../components/Commands/CheckOption';
import UncheckOption from '../components/Commands/UncheckOption';
import WaitForElement from '../components/Commands/WaitForElement';

const availableCommands = {
    default: {
        Click: {
            buttonIcon: '<i class="fa fa-mouse-pointer" aria-hidden="true"></i>'
            , buttonclass: 'atf-rec-command-click'
        }
        , grGrabab: {
            buttonIcon: '<i class="fa fa-hand-rock-o" aria-hidden="true"></i>'
            , buttonclass: 'atf-rec-command-grab'
        }
        , PressKey: {
            buttonIcon: '<i class="fa fa-keyboard-o" aria-hidden="true"></i>'
            , buttonclass: 'atf-rec-command-presskey'
        }
        , See: {
            buttonIcon: '<i class="fa fa-eye" aria-hidden="true"></i>'
            , buttonclass: 'atf-rec-command-see'
        }
        , WaitForElement: {
            buttonIcon: '<i class="fa fa-clock-o" aria-hidden="true"></i>'
            , buttonclass: 'atf-rec-command-waitfor'
        }
    }
}

export default class CommandBuilder {
    constructor(properties = {}) {
        this._properties = Object.assign({}, properties);
        this._commands = [];

        let _this = this;

        this._properties.commandsContainer.find('.atf-rec-build-commands').sortable({
            stop: function(ev, ui) {
                let newOrder = [];

                $(this).find('> li.atf-rec-built-command-container').each((index, element) => {
                    _this._commands.forEach(command => {
                        if (command == $(element).data('atf-rec-command')) {
                            newOrder.push(command);
                        }
                    });
                });

                _this._commands = newOrder;
            }
        });
        this._properties.commandsContainer.find('.atf-rec-build-commands').disableSelection(); // This is irrelevant
        this._additionalCommandsContainer = this._properties.commandsContainer.find('.atf-rec-commands-available-actions > .atf-rec-commands-list').empty();
        
        this.__createAdditionalCommandsButtons();

        this.__bindListeners();
        return this;
    }

    get selector() {
        return this._properties.selector;
    }

    get value() {
        return this._properties.value;
    }

    get inputValue() {
        return this._properties.inputValue;
    }

    __createAdditionalCommandsButtons() {
        for(let command in availableCommands.default) {
            let buttonIcon, buttonClass;

            let commandButton = $(`
                <button type="button" class="atf-rec-button atf-rec-green-button ${availableCommands.default[command].buttonClass}">
                    ${availableCommands.default[command].buttonIcon}
                </button>
            `);

            commandButton
                .off('click.atf-rec')
                .on('click.atf-rec', function(ev) {
                    this._triggerCommand(this.add({
                        action: command
                        , selector: this._properties.selector
                        , value: this._properties.value
                    }));

                    return false;
                }.bind(this));

            this._additionalCommandsContainer.append(commandButton);

        };
    }

    _triggerCommand(command) {
        switch (command.type) {
            case 'click':
                setTimeout(() => {
                    this._properties.element.click();
                }, 1000);
                break;
        }
    }

    add(props) {
        let command = null
            , _this = this;

        switch (props.action.toLowerCase()) {
            case 'click':
                command = this.push(new Click({
                    selector: this.selector
                    , value: this.value.split(" ").slice(0, 10).join(" ")
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'see':
                command = this.push(new See({
                    selector: this.selector
                    , value: this.value.split(" ").slice(0, 10).join(" ")
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'presskey':
                command = this.push(new PressKey({
                    selector: this.selector
                    , value: ''
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'seeinfield':
                command = this.push(new SeeInField({
                    selector: this.selector
                    , value: this.inputValue
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'fillfield':
                command = this.push(new FillField({
                    selector: this.selector
                    , value: this.inputValue
                    , onChange: props.onChange
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'selectoption':
                command = this.push(new SelectOption({
                    selector: this.selector
                    , value: this.inputValue
                    , element: this._properties.element
                    , onChange: props.onChange
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'seecheckboxischecked':
                command = this.push(new SeeCheckBoxIsChecked({
                    selector: this.selector
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'checkoption':
                command = this.push(new CheckOption({
                    selector: this.selector
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'uncheckoption':
                command = this.push(new UncheckOption({
                    selector: this.selector
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;

            case 'waitforelement':
                command = this.push(new WaitForElement({
                    selector: this.selector
                    , onRemove(command) {
                        _this.__commandRemoved(command);
                    }
                }, props.default));
                break;
        }

        return command;
    }

    get getCommands() {
        return this._commands;
    }

    push(command) {
        this._commands.push(command);
        command.get().data('atf-rec-command', command);
        this._properties.commandsContainer.find('.atf-rec-build-commands').append(command.get());

        return command;
    }

    __commandRemoved(command) {
        this._commands.splice(this._commands.indexOf(command), 1);
        return true;
    }

    __bindListeners() {
        
    }
}