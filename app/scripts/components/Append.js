const $ = jQuery = require('jquery');
require('../utils/jquery.helpers');

let template = $(`
    <div class="atf-rec-append-text-container">
        <label class="atf-rec-form-label" for="atf-rec-append-text-field">Append text</label>
        <input type="text" class="atf-rec-form-field" id="atf-rec-append-text-field">
    </div>
    <div class="atf-rec-buttons-group">
        <button type="button" class="atf-rec-button atf-rec-undo-action">
            <i class="fa fa-refresh" aria-hidden="true"></i>
        </button>
        <button class="atf-rec-button atf-rec-append-action">Append</button>
    </div>
`);

export default class Append {
    constructor(properties = {}, container) {
        this.template = template.clone();
        this.properties = Object.assign({}, properties);

        this.appendedText = '';

        this.container = container;
        this.container.empty().html(this.template);

        this.template.find('.atf-rec-undo-action').prop('disabled', true);

        this.__bindListeners();
    }

    __bindListeners() {
        let _this = this;

        this.template.find('.atf-rec-append-action')
            .off('click.atf-rec')
            .on('click.atf-rec', function(ev) {
                ev.stopPropagation();
                ev.preventDefault();
                if (_this.template.find('#atf-rec-append-text-field').val()) {
                    _this.appendedText = _this.template.find('#atf-rec-append-text-field').val();
                    
                    let lastChildWithText = _this.properties.element.find(':hasText').last()
                        , appendTo = null;

                    if (lastChildWithText.length) {
                        appendTo = lastChildWithText;
                    } else if (_this.properties.element.text()) {
                        appendTo = _this.properties.element;
                    }

                    if (appendTo) {
                        let text = appendTo.clone().children().remove().end().text();
                        appendTo.html(appendTo.html().replace(text, text + _this.appendedText));
                    }

                    _this.template.find('#atf-rec-append-text-field').val('');
                    _this.template.find('.atf-rec-undo-action').prop('disabled', false);
                }

                return false;
            });

        this.template.find('.atf-rec-undo-action')
            .off('click')
            .on('click', function(ev) {
                ev.stopPropagation();
                ev.preventDefault();

                let lastChildWithText = _this.properties.element.find(':hasText').last()
                    , removeFrom = null;

                if (lastChildWithText.length) {
                    removeFrom = lastChildWithText;
                } else if (_this.properties.element.text()) {
                    removeFrom = _this.properties.element;
                }

                _this.template.find('#atf-rec-append-text-field').val('');

                _this.properties.element.html(_this.properties.element.html().substr( 0, _this.properties.element.text().lastIndexOf(_this.appendedText)));

                _this.template.find('.atf-rec-undo-action').prop('disabled', true);

                return false;
            });
    }
}