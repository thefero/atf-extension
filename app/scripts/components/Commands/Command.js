let template = $(`
    <li class="atf-rec-built-command-container">
        <div class="atf-rec-built-command-disabled">Disabled</div>
        <div class="atf-rec-built-command">{{command}}</div>
        <div class="atf-rec-command-actions">
            <button role="button" class="atf-rec-hover-button atf-rec-set-build-command atf-rec-ok">
                <i class="fa fa-check" aria-hidden="true"></i>
            </button>
            <button role="button" class="atf-rec-hover-button atf-rec-remove-build-command atf-rec-del">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </li>
`);

export default class Command {
    constructor(properties = {}, disabled = false) {
        this._properties = Object.assign({
            selector: null
            , value: null
            , removed: false
            , onRemove: function() {}
        }, properties);

        this.selector = this._properties.selector;
        this.value = this._properties.value;

        this._template = template.clone();

        this._commandBuildSection = this._template.find('.atf-rec-built-command');
        this._commandDisabledSection = this._template.find('.atf-rec-built-command-disabled');
        this._commandActionsSection = this._template.find('> .atf-rec-command-actions');

        this._defaultCommand = disabled || false;

        if (!this._defaultCommand) {
            this._commandActionsSection.find('.atf-rec-set-build-command').remove();
            this._commandDisabledSection.remove();
        }

        this.__bindListeners();
    }

    render() {
        this._commandBuildSection.empty().html(this._commandTemplate);
    }

    setSelector(selector = '') {
        this._properties.selector = selector;
        this._template.find('.atf-rec-selector').text(selector);

        return this.__selectorChanged();
    }

    setValue(value = '') {
        this.value = value;
        this._template.find('.atf-rec-value > input[type="text"]').val(value);

        return this.__valueChanged();
    }

    getValue() {
        return this.value;
    }

    getSelector() {
        return this.selector;
    }

    get() {
        return this._template;
    }

    get type() {
        return this._commandType.toLowerCase();
    }

    __selectorChanged() {
        return this;
    }

    __valueChanged() {
        return this;
    }

    get isDisabled() {
        return this._defaultCommand;
    }

    removeCommand() {
        this._template.hide('fast');
        this.removed = true;

        if (typeof this._properties.onRemove === 'function') {
            this._properties.onRemove(this);
        }
        return false;
    }

    __makeCommandAvailable() {
        this._commandDisabledSection.hide('fast').remove();
        this._commandActionsSection.find('.atf-rec-set-build-command').hide('fast');
        this._defaultCommand = false;

        return false;
    }

    __bindListeners() {
        let _this = this;
        this._commandActionsSection.find('.atf-rec-set-build-command')
            .off('click.atf-rec')
            .on('click.atf-rec', function(ev) {
                _this.__makeCommandAvailable();
            });
        this._commandActionsSection.find('.atf-rec-remove-build-command')
            .off('click.atf-rec')
            .on('click.atf-rec', function(ev) {
                _this.removeCommand();
            });
    }
}