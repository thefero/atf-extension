import Command from './Command';

let uncheckOptionCommand = $(`
	<div>
		<span class="atf-rec-actor">$I-></span><span class="atf-rec-action atf-rec-badge atf-rec-badge-secondary"></span>(<span class="atf-rec-selector-container">"<span class="atf-rec-selector"></span>"</span>);
	</div>
`);

export default class UncheckOption extends Command {
	constructor(properties = {}, disabled = false) {
		super(properties, disabled);

		this._commandType = 'uncheckOption';
		this._commandIndex = 0;
		this._commandTemplate = uncheckOptionCommand.clone();

		this.__init(this._properties);
	}

	__init(options) {
		this._commandTemplate.find('.atf-rec-action').text(this._commandType);

		this._commandTemplate.find('.atf-rec-selector').text(this.selector);

		this.render();
	}
}