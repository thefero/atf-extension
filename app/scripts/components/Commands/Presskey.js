import Command from './Command';

let preeKeyCommand = $('<div><span class="atf-rec-actor">$I-></span><span class="atf-rec-action atf-rec-badge atf-rec-badge-secondary">pressKey</span>("<span class="atf-rec-selector">selector-here</span>", <span class="atf-rec-value"><input type="text" value=""></span>);</div>');

export default class Presskey extends Command {
	constructor(properties = {}, disabled = false) {
		super(properties, disabled);

		this._commandType = 'click';
		this._commandTemplate = preeKeyCommand.clone();

		this.__init(this._properties);

		return this;
	}

	__init() {
		let specialPressed = false
			, text = []
			, _this = this;

		this._commandTemplate.find('.atf-rec-selector').text(this.selector);
		this._commandTemplate.find('.atf-rec-value > input[type="text"]')
            .off('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec')
            .on('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec', function(ev) {

            	if (typeof ev.key === 'undefined') {
            		return false;
            	}

            	if (['Alt', 'Control', 'Shift'].indexOf(ev.key) >= 0) {
                	specialPressed = true;

                	if (ev.key == 'Control') {
						text.push('Ctrl');
					}

					if (ev.key == 'Alt') {
						text.push('Alt');
					}

					if (ev.key == 'Shift') {
						text.push('Shift');
					}
            		
            		return false;
            	} else {
            		text.push(ev.key);
            		$(this).val(text.join(' + '));
            		_this._properties.value = text.join(' + ');

                	specialPressed = false;
                	text = [];
            	}

            	ev.preventDefault();
            	ev.stopPropagation();
            });
            
        this.render();
	}
}