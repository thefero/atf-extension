import Command from './Command';

let seeCheckBoxIsCheckedCommand = $(`
	<div>
		<span class="atf-rec-actor">$I-></span><a href="#" class="atf-rec-command-switcher"><span class="atf-rec-action atf-rec-badge atf-rec-badge-secondary"></span></a>(<span class="atf-rec-selector-container">"<span class="atf-rec-selector"></span>"</span>);
	</div>
`);

const commands = ['seeCheckBoxIsChecked', 'dontSeeCheckBoxIsChecked'];

export default class See extends Command {
	constructor(properties = {}, disabled = false) {
		super(properties, disabled);

		this._commandType = 'seeCheckBoxIsChecked';
		this._commandIndex = 0;
		this._commandTemplate = seeCheckBoxIsCheckedCommand.clone();

		this.__init(this._properties);
	}

	__init(options) {
		this._commandTemplate.find('.atf-rec-action').text(this._commandType);

		this._commandTemplate.find('.atf-rec-selector').text(this.selector);

		let _this = this;

		this._commandTemplate.find('.atf-rec-command-switcher')
			.off('click.atf-rec')
			.on('click.atf-rec', function(ev) {
				ev.preventDefault();
				ev.stopPropagation();

				_this.__switchCommand();

				return false;
			});

		this.render();
	}

	__switchCommand() {
		++this._commandIndex;
		if (this._commandIndex == commands.length) {
			this._commandIndex = 0;
		}

		this._commandType = commands[this._commandIndex];
 
		this._commandTemplate.find('.atf-rec-action').text(this._commandType);

		return this;
	}

	setValue(value) {
		return super.setValue(value);
	}
}