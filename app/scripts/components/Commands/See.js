import Command from './Command';

let seeCommand = $(`
	<div>
		<span class="atf-rec-actor">$I-></span><a href="#" class="atf-rec-command-switcher"><span class="atf-rec-action atf-rec-badge atf-rec-badge-secondary"></span></a>("<span class="atf-rec-value"><input type="text" value=""></span>"<span class="atf-rec-selector-container">, "<span class="atf-rec-selector"></span>"</span>);
		<div class="atf-rec-command-view-switcher">
			<span>
				<input id="atf-rec-see-command-view-switcher-1" type="radio" name="atf-rec-command-view-choice" class="atf-rec-font-input" checked data-value="1" value="1">
				<label for="atf-rec-see-command-view-switcher-1"></label>
			</span>
			<span>
				<input id="atf-rec-see-command-view-switcher-2" type="radio" name="atf-rec-command-view-choice" class="atf-rec-font-input" data-value="2" value="2">
				<label for="atf-rec-see-command-view-switcher-2"></label>
			</span>
		</div>
	</div>
`);

const commands = ['see', 'dontSee'];

export default class See extends Command {
	constructor(properties = {}, disabled = false) {
		super(properties, disabled);

		let unique = Math.random().toString(36).substr(2, 9);

		this._commandType = 'see';
		this._commandIndex = 0;
		this._commandTemplate = seeCommand.clone();

		this._commandTemplate.find('input[name=atf-rec-command-view-choice]')
			.prop('name', 'atf-rec-command-view-choice-' + unique);

		this.__init(this._properties);
	}

	__init(options) {
		this._commandTemplate.find('.atf-rec-action').text(this._commandType);

		this._commandTemplate.find('.atf-rec-selector').text(this.selector);
		this._commandTemplate.find('.atf-rec-selector-container').hide();
		this._commandTemplate.find('.atf-rec-value').text(this.value);

		let _this = this;

		this._commandTemplate
			.find('.atf-rec-command-view-switcher input[type="radio"].atf-rec-font-input')
				.off('change.atf-rec')
				.on('change.atf-rec', function(ev) {
					this[`__display${$(ev.target).val()}`]();
				}.bind(this));

		this._commandTemplate.find('.atf-rec-command-switcher')
			.off('click.atf-rec')
			.on('click.atf-rec', function(ev) {
				ev.preventDefault();
				ev.stopPropagation();

				_this.__switchCommand();

				return false;
			});

		this.render();
	}

	__display1() {
		this._commandTemplate.find('.atf-rec-selector-container').hide("fade", "fast");
	}

	__display2() {
		this._commandTemplate.find('.atf-rec-selector-container').show("fade", "fast");
	}

	__switchCommand() {
		++this._commandIndex;
		if (this._commandIndex == commands.length) {
			this._commandIndex = 0;
		}

		this._commandType = commands[this._commandIndex];
 
		this._commandTemplate.find('.atf-rec-action').text(this._commandType);

		let _this = this;

		switch (this._commandIndex) {
			case 1:
				this._commandTemplate.find('.atf-rec-value').html(
					$(`<input>`)
						.prop('type', 'text')
						.val('')
						.off('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec')
            			.on('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec', function(ev) {
            				_this.setValue($(this).val());
            			})
				);

				this.setValue('');

				break;
			default:
				this._commandTemplate.find('.atf-rec-value').text(this._properties.value);
				this.setValue(this._properties.value);
		}

		return this;
	}

	setValue(value) {
		return super.setValue(value);
	}
}