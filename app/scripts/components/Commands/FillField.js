import Command from './Command';

let fillFieldCommand = $('<div><span class="atf-rec-actor">$I-></span><span class="atf-rec-action atf-rec-badge atf-rec-badge-secondary">fillField</span>("<span class="atf-rec-selector">selector-here</span>", <span class="atf-rec-value"><input type="text" value=""></span>);</div>');

export default class FillField extends Command {
    constructor(properties = {}, disabled = false) {
        super(properties, disabled);

        this._commandType = 'fillfield';
        this._commandTemplate = fillFieldCommand.clone();

        this.__init(this.properties);

        return this;
    }

    __init(properties) {
        let _this = this;

        this._commandTemplate.find('.atf-rec-selector').text(this.selector);
        this._commandTemplate.find('.atf-rec-value > input[type="text"]')
            .val(_this.value)
            .off('propertychange.atf-rec change.atf-rec focus.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec')
            .on('propertychange.atf-rec change.atf-rec focus.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec', function(ev) {
                if (_this._defaultCommand) {
                    _this.__makeCommandAvailable();
                }
                
                _this.setValue($(this).val());

                if (typeof _this._properties.onChange === 'function') {
                    _this._properties.onChange($(this).val())
                }
            });

        this.render();
    }

    setValue(value) {
        this._commandTemplate.find('.atf-rec-value > input[type="text"]').val(value);
        return super.setValue(value);
    }
}