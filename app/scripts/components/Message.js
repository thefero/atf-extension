export default class Message {
    constructor() {
        return this;
    }

    sendCommand(event) {
        let message = Object.assign({
            from: 'content'
        }, event);

        chrome.runtime.sendMessage(message);
    }
}