import '../utils/domPath';

export default class Selectors {
    constructor(properties = {}, container) {
        this._properties = Object.assign({}, properties);

        let $element = $(this._properties.element);
        this._element = {
            node: $element
            , domPath: $element.getDomPath()
            , seenText: $.trim($element.clone().children().remove().end().text())
        };

        return this;
    }

    get element() {
        return this._element.node;
    }

    get text() {
        return this._element.seenText;
    }

    get val() {
        return this._element.node.val() || '';
    }

    get cssPath() {
        return this._element.domPath;
    }
}