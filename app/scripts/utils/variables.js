window.$ = window.jQuery = require('jquery');
import modalTemplate from '../templates/modal';
import modalPopoverTemplate from '../templates/modalPopover';
import appendTextTemplate from '../templates/appendTextForm';

let modalTemplateJQ = $(modalTemplate);

export default {
    overlayModal: modalTemplateJQ
    , overlayModalPopover: $(modalPopoverTemplate)
    , actionsForm: $(appendTextTemplate)
    , modalTop: modalTemplateJQ.find('.atf-rec-overlay-modal-top')
    , modalBottom: modalTemplateJQ.find('.atf-rec-overlay-modal-bottom')
    , innerMarginWidth: 3
    , modalTopHeight: 39
}