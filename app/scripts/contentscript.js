const $ = jQuery = require('jquery');
require('jquery.scrollbar');
import Popper from 'popper.js';
window.Popper = Popper;
import Tooltip from 'tooltip.js';
window.Tooltip = Tooltip;
import vars from './utils/variables';
import htmlElements from './utils/htmlElements';
import Message from './components/Message';
import CommandBuilder from './components/CommandBuilder';
import EventBus from './components/EventBus';

import Append from './components/Append';
import Selectors from './components/Selectors';
import ElementTags from './components/ElementTags';

let modalPopover
    , commandsBuilder = null;

let activateExtension = () => {
    $('body').append(vars.overlayModal.hide()).append(vars.overlayModalPopover.hide());

    $(document)
        .on('mouseover.atf-rec', function(ev) {
            hoverListener(ev);
        })
        .on('mouseout.atf-rec', function(ev) {
            $(prevSelectedElement).off('click.atf-rec');
        });

    modalPopover = new Popper(
        vars.overlayModal
        , vars.overlayModalPopover
        , {
            placement: 'right'
            , onCreate(data) {
                $('.atf-rec-inline-actions-body').scrollbar();
            }
            , modifiers: {
                flip: {
                    behavior: ['right', 'bottom', 'top', 'left']
                }
                , arrow: {
                    element: '.atf-rec-modal-popover-arrow'
                }
            }
        }
    );
}

chrome.runtime.sendMessage({
    type: 'getStatus'
}, function (status) {
    if (!status) {
        vars.overlayModal.remove();
        vars.overlayModalPopover.remove();

        $(document).off('mouseover.atf-rec');
    } else {
        activateExtension()
    }
});

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (typeof msg.type !== 'undefined' && msg.type === 'status') {
        if (!msg.value) {
            vars.overlayModal.remove();
            vars.overlayModalPopover.remove();
        } else {
            activateExtension()
        }
    }
});

let selectedElement
    , prevSelectedElement
    , animateHighlight
    , appendedText = ''
    , elementAttributestsList = vars.overlayModalPopover.find('.atf-rec-element-tags-list');

let message = new Message();

let sendElementCommands = () => {
    if (commandsBuilder && commandsBuilder.getCommands.lenght) {
        let pushCommands = [];

        commandsBuilder.getCommands.forEach(command => {
            if (!command.isDisabled) {
                pushCommands.push(command);
                command.removeCommand();
            }
        });

        message.sendCommand({
            commands: pushCommands
        });
    }
    return true;
}

let hoverListener = (ev) => {
    clearTimeout(animateHighlight);
    appendedText = '';

    animateHighlight = setTimeout(function() {
        selectedElement = ev.target;
        let $element = $(selectedElement);

        if (
            $element &&
            $element.prop('tagName').toLowerCase() != 'body' &&
            !$.contains(vars.overlayModal[0], selectedElement) &&
            !$element.hasClass('atf-rec-overlay-modal') &&
            !$.contains(vars.overlayModalPopover[0], selectedElement) &&
            !$element.hasClass('atf-rec-modal-popover') &&
            selectedElement != prevSelectedElement
        ) {
            let $element = $(selectedElement)
                , noOtherCommands = true
                , command
                , seenText = $.trim($element.clone().children().remove().end().text())
                , selectOptionCommand = null
                , checkOptionCommand = null
                , uncheckOptionCommand = null
                , fillfieldCommand = null;

            vars.overlayModalPopover.find('.atf-rec-inline-actions-body .atf-rec-build-commands').empty();

            vars.overlayModal.off('mouseover.atf-rec').find('*').off('mouseover.atf-rec');
            vars.overlayModalPopover.off('mouseover.atf-rec').find('*').off('mouseover.atf-rec');
            vars.overlayModal.show();
            vars.overlayModalPopover.show();

            if (prevSelectedElement) {
                $(prevSelectedElement).trigger('blur');
            }
            sendElementCommands();
            prevSelectedElement = selectedElement;

            positionModal();
            modalPopover.update();

            new ElementTags({
                element: selectedElement
                , container: vars.overlayModalPopover.find('.atf-rec-element-tags-container')
            });

            let selectors = new Selectors({
                element: prevSelectedElement
            }, vars.overlayModalPopover.find('.atf-rec-selectors-container'));

            let elementVal = selectors.text;
            if (['select', 'input', 'textarea', 'datalist', 'keygen'].indexOf(selectedElement.nodeName.toLowerCase()) >= 0) {
                elementVal = selectors.val;
            }

            commandsBuilder = new CommandBuilder({
                element: selectedElement
                , commandsContainer: vars.overlayModalPopover.find('.atf-rec-inline-actions-body .atf-rec-commands')
                , selector: selectors.cssPath
                , value: selectors.text
                , inputValue: selectors.val
            });

            if (selectors.text) {
                commandsBuilder.add({
                    action: 'see'
                    , default: true
                });
            }

            if ((selectedElement.nodeName.toLowerCase() == 'input' && $element.prop("type") != 'radio' && $element.prop("type") != 'chekcbox') || selectedElement.nodeName.toLowerCase() != 'input') {
                $element
                    .off('click.atf-rec')
                    .on('click.atf-rec', function(ev) {
                        commandsBuilder.add({
                            action: 'click'
                        });
                    });
            }

            
            if (['select', 'input', 'textarea', 'datalist', 'keygen'].indexOf(selectedElement.nodeName.toLowerCase()) >= 0) {
                if (!(selectedElement.nodeName.toLowerCase() == 'input' && ($element.prop("type") == 'checkbox' || $element.prop("type") == 'radio'))) {
                    commandsBuilder.add({
                        action: 'seeinfield'
                        , default: true
                    });
                }

                if (['input', 'textarea', 'datalist', 'keygen'].indexOf(selectedElement.nodeName.toLowerCase()) >= 0) {
                    if (selectedElement.nodeName.toLowerCase() == 'input' && ($element.prop("type") == 'checkbox' || $element.prop("type") == 'radio')) {

                        if ($element.prop("type") == 'checkbox') {
                            $element
                                .off('propertychange.atf-rec change.atf-rec')
                                .on('propertychange.atf-rec change.atf-rec', function() {
                                    let $this = $(this);

                                    if ($(this).is(':checked')) {
                                        if (!checkOptionCommand) {
                                            checkOptionCommand = commandsBuilder.add({
                                                action: 'checkoption'
                                            });
                                        }
                                    } else {
                                        if (!uncheckOptionCommand) {
                                            uncheckOptionCommand = commandsBuilder.add({
                                                action: 'uncheckoption'
                                            });
                                        }
                                    }

                            });
                        
                            commandsBuilder.add({
                                action: 'seecheckboxischecked'
                                , default: true
                                , onChange(newVal) {
                                    $element.val(newVal)
                                }
                            });
                        } else {
                            if ($element.prop("type") == 'radio') {
                                $element
                                    .off('propertychange.atf-rec change.atf-rec')
                                    .on('propertychange.atf-rec change.atf-rec', function() {
                                        let $this = $(this);

                                        if (!selectOptionCommand) {
                                            selectOptionCommand = commandsBuilder.add({
                                                action: 'selectoption'
                                            });
                                        }

                                    });
                            }
                        }
                    } else {
                        $element
                            .off('propertychange.atf-rec change.atf-rec focus.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec')
                            .on('propertychange.atf-rec change.atf-rec focus.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec', function() {
                                let $this = $(this);

                                if (fillfieldCommand) {
                                    fillfieldCommand.setValue($element.val());
                                }
                        });

                        fillfieldCommand = commandsBuilder.add({
                            action: 'fillfield'
                            , default: true
                            , onChange(newVal) {
                                $element.val(newVal)
                            }
                        });
                    }
                }

                if (['select'].indexOf(selectedElement.nodeName.toLowerCase()) >= 0) {

                    $element
                        .off('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec')
                        .on('propertychange.atf-rec change.atf-rec keyup.atf-rec input.atf-rec paste.atf-rec', function() {
                            let $this = $(this);

                            if (selectOptionCommand) {
                                selectOptionCommand.setValue($element.val());
                            }
                    });

                    selectOptionCommand = commandsBuilder.add({
                        action: 'selectoption'
                        , default: true
                        , onChange(newVal) {
                            $element.val(newVal)
                        }
                    });
                }
            }

            vars.overlayModalPopover.find('.atf-rec-send-commdans')
                .off('click')
                .on('click', function(ev) {
                    sendElementCommands();
                });

        }
    }, 300);
}

let positionModal = () => {
    if (selectedElement) {
        let elPorps = selectedElement.getBoundingClientRect();
        let $selectedElement = $(selectedElement)
            , elOffset = $selectedElement.offset();

        vars.overlayModal
            .css({
                width: elPorps.width + vars.innerMarginWidth + 2,
                marginLeft: elOffset.left - 3,
                marginTop: elOffset.top - 3,
                height: elPorps.height + 5
            });
    }
}