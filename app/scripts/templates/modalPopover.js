export default `
    <div class="atf-rec-modal-popover">
        <div class="atf-rec-append-container"></div>
        <div class="atf-rec-element-tags-container"></div>
        <div class="atf-rec-inline-actions-body">
            <div class="atf-rec-selectors-container"></div>
            <div class="atf-rec-commands">
                <div class="atf-rec-title">Commands <a href="#" class="atf-rec-info"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div>
                <ul class="atf-rec-build-commands"></ul>
                <div class="atf-rec-commands-available-actions">
                    <div class="atf-rec-title-small">Add command</div>
                    <div class="atf-rec-buttons-group atf-rec-commands-list"></div>
                </div>
                <button role="button" class="atf-rec-button atf-rec-send-commdans">Save commands</button>
            </div>
		</div>
        <div class="atf-rec-modal-popover-arrow" x-arrow></div>
    </div>
`;