export default `
    <div>
        <div class="form-group row">
            <label for="atf-rec-append-text-field" class="col-sm-2 col-form-label">Append</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="atf-rec-form-control" id="atf-rec-append-text-field" placeholder="Your text">
                    <span class="input-group-btn atf-rec-undo-continer">
                        <button class="btn btn-warning atf-rec-undo-action" type="button">
                            <i class="fa fa-undo" aria-hidden="true"></i>
                        </button>
                    </span>
                    <span class="input-group-btn">
                        <button class="btn btn-primary atf-rec-append-action" type="button">Append</button>
                    </span>
                </div>

            </div>
        </div>
    </div>
`;