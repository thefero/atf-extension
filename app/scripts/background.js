// Enable chromereload by uncommenting this line:
// import 'chromereload/devonly'

chrome.runtime.onInstalled.addListener((details) => {
  console.log('previousVersion', details.previousVersion)
});

chrome.tabs.onUpdated.addListener((tabId) => {
  chrome.pageAction.show(tabId)
});

let extensionEnabled = false
    , commands = [];

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {

    if (typeof msg.type !== 'undefined') {
        switch (msg.type) {
            case 'getCommands':
                sendResponse(commands);
                break;
            case 'removeCommand':
                commands.forEach((command, index) => {
                    if (
                        command.action == msg.command.action &&
                        command.selector == msg.command.selector &&
                        command.value == msg.command.value
                    ) {
                        commands.splice(index, 1);
                        return false;
                    }
                });

                break;
            case 'changeCommand':
                commands.forEach((command, index) => {
                    if (
                        command.action == msg.originalCommand.action &&
                        command.selector == msg.originalCommand.selector &&
                        command.value == msg.originalCommand.value
                    ) {
                        command.selector = msg.newCommand.selector
                        return false;
                    }
                });

                break;
            case 'setStatus':
                extensionEnabled = msg.value || false;

                if (!extensionEnabled) {
                    commands = [];
                }

                chrome.tabs.query({}, function(tabs) {
                    tabs.forEach((tab) => {
                        chrome.tabs.sendMessage(tab.id, {
                            type: 'status'
                            , value: extensionEnabled
                        }, function(response) {

                        });
                    });
                });
                
                sendResponse(extensionEnabled);

                break;
            case 'getStatus':
                sendResponse(extensionEnabled);
                break;
        }
    }

    if (msg.from === 'content' || msg.from === 'popup') {
        if (typeof msg.commands !== 'undefined') {
            msg.commands.forEach(command => {
                commands.push(command);
            });
        }
    }
});